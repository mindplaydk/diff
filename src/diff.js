import createIndex from "./createIndex.js";

export default function diff(parent, oldChildren, newChildren, endMarker) {
  console.log("diffing from", oldChildren.map(n => n.textContent), "to", newChildren.map(n => n.textContent))

  // remove old children:

  let newIndex = createIndex(newChildren);

  oldChildren = oldChildren.filter(child => {
    if (newIndex.has(child)) {
      return true;
    }

    console.log("- remove child: ", child.textContent);

    parent.removeChild(child);

    return false;
  });

  console.log("diffing from", oldChildren.map(n => n.textContent), "to", newChildren.map(n => n.textContent))

  // find ranges:

  let range = [];
  let ranges = [range]; // TODO improve handling when `oldChildren == []` - we currently end up with a useless empty range
  let addedChildren = [];

  let oldIndex = createIndex(oldChildren);

  newIndex.set(endMarker, newChildren.length);
  
  for (let i=0; i<newChildren.length; i++) {
    let child = newChildren[i];
    
    if (oldIndex.has(child)) {
      if (newChildren[i-1] == oldChildren[oldIndex.get(child) - 1]) {
        range.push(child);
      } else {
        ranges.push(range = [child]);
      }
    } else {
      addedChildren.push(child);
    }
  }

  ranges.sort(
  	(a, b) => a.length !== b.length
	  	? a.length - b.length // shorter ranges first
	    : (newIndex.get(a[0]) - newIndex.get(b[0])) // lower indices first
  );

  // process ranges:

  let children = parent.childNodes;
  let childrenStart = Array.prototype.indexOf.call(children, oldChildren[0]);
  let childrenEnd = childrenStart + oldChildren.length;

  console.log("- found ranges:", ranges.map(a => a.map(n => newIndex.get(n))));

  for (let i=0; i<ranges.length; i++) {
    let range = ranges[i];

    console.log("- processing range:", range.map(n => n.textContent));

    let last = range[range.length - 1];
    let lastIndex = newIndex.get(last);

    for (let n=childrenStart; n<=childrenEnd; n++) {
      let next = children[n] || endMarker;

      //console.log("- test ", newIndex.get(next), ">", lastIndex, "?");

      if (newIndex.get(next) > lastIndex) {
        if (last.nextSibling != next) {
          console.log("- moving range:", range.map(n => n.textContent), "before", next ? next.textContent : null);

          for (let c=range.length-1; c>=0; c--) {
            let child = range[c];

            console.log("- insert child", child.textContent, "before", next ? next.textContent : null);

            parent.insertBefore(child, next);

            next = child;
          }

          console.log("- DOM state is:", children.map(n => n.textContent));
        } else {
          console.log("- skip moving range:", range.map(n => n.textContent), "is already located before", next ? next.textContent : null);
        }

        break;
      }
    }
  }

  // process added children:

  for (let i=addedChildren.length; i--;) {
    const child = addedChildren[i];
    const next = newChildren[newIndex.get(child) + 1] || endMarker;

    console.log("- insert new child", child.textContent, "before", next ? next.textContent : null);

    parent.insertBefore(child, next);
  }
}
