export default function createIndex(children) {
  let map = new Map();

  for (let i=children.length; i--;) {
    map.set(children[i], i);
  }
  
  return map;
}
