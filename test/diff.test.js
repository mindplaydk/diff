import { test } from "zora";
import undom from "./undom/undom.js";
import diff from "../src/diff.js";

test("can reorder DOM children", is => {
  let cases = [
    [
      "append to empty range",
      [],
      [0, 1, 2],
      { insertions: 3 }
    ],
    [
      "remove entire range",
      [0, 1, 2],
      [],
      { removals: 3 }
    ],
    [
      "shuffle mixed ranges",
      [3, 4, 5, 1, 2, 6, 0],
      [0, 1, 2, 3, 4, 5, 6],
      { insertions: 3 }
    ],
    [
      "rotate up",
      [3, 0, 1, 2],
      [0, 1, 2, 3],
      { insertions: 1 }
    ],
    [
      "rotate down",
      [1, 2, 3, 0],
      [0, 1, 2, 3],
      { insertions: 1 }
    ],
    [
      "reverse",
      [3, 2, 1, 0],
      [0, 1, 2, 3],
      { insertions: 3 }
    ],
    [
      "append one",
      [0, 1, 2],
      [0, 1, 2, 3],
      { insertions: 1 }
    ],
    [
      "append two",
      [0, 1, 2],
      [0, 1, 2, 3, 4],
      { insertions: 2 }
    ],
    [
      "preprend one",
      [0, 1, 2],
      [3, 0, 1, 2],
      { insertions: 1 }
    ],
    [
      "prepend two",
      [0, 1, 2],
      [3, 4, 0, 1, 2],
      { insertions: 2 }
    ],
    [
      "remove one from head",
      [0, 1, 2, 3],
      [1, 2, 3],
      { removals: 1 }
    ],
    [
      "remove two from head",
      [0, 1, 2, 3],
      [2, 3],
      { removals: 2 }
    ],
    [
      "remove from head, append to tail",
      [0, 1, 2, 3],
      [1, 2, 3, 4],
      { insertions: 1, removals: 1 }
    ],
    [
      "remove one from tail",
      [0, 1, 2, 3],
      [0, 1, 2],
      { removals: 1 }
    ],
    [
      "remove two from tail",
      [0, 1, 2, 3],
      [0, 1],
      { removals: 2 }
    ],
    [
      "remove from tail, append to head",
      [0, 1, 2, 3],
      [4, 0, 1, 2],
      { insertions: 1, removals: 1 }
    ],
    [
      "replace odd elements",
      [0, 1, 2, 3, 4],
      [5, 1, 6, 3, 7],
      { insertions: 3, removals: 3 }
    ],
    [
      "replace even elements",
      [0, 1, 2, 3, 4],
      [0, 5, 2, 6, 4],
      { insertions: 2, removals: 2 }
    ],
  ];
  
  for (let [title, from, to, expected] of cases) {
    let document = undom();

    let parent = document.body;

    // build the old DOM state and new child list for the test:

    let uniqueChildren = new Map();

    function uniqueChild(i) {
      if (! uniqueChildren.has(i)) {
        uniqueChildren.set(i, document.createTextNode(i));
      }

      return uniqueChildren.get(i);
    }

    let oldChildren = from.map(uniqueChild);
    let newChildren = to.map(uniqueChild);

    oldChildren.forEach(child => { parent.appendChild(child) });

    // install a spy, so we can count the number of DOM insertions:
  
    let insertions = 0;

    let insertBefore = parent.insertBefore.bind(parent);

    parent.insertBefore = (child, ref) => {
      insertions += 1;
      return insertBefore(child, ref);
    };

    // install a spy, so we can count the number of DOM removals:

    let removals = 0;

    let removeChild = parent.removeChild.bind(parent);

    parent.removeChild = (child) => {
      removals += 1;
      return removeChild(child);
    };

    // perform the test:
  
    console.log("------ " + title);

    diff(parent, oldChildren, newChildren);
  
    is.equal(
      [...parent.childNodes].map(n => n.textContent),
      newChildren.map(n => n.textContent),
      title
    );

    is.equal(
      insertions,
      expected.insertions || 0,
      `${title} completes with ${expected.insertions || 0} insertions`
    );

    is.equal(
      removals,
      expected.removals || 0,
      `${title} completes with ${expected.removals || 0} removals`
    );
  }
});
