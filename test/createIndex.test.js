import { test } from "zora";

import createIndex from "../src/createIndex.js";

test("can create index", is => {
  let A = {};
  let B = {};
  let C = {};
  let children = [A, B, C];

  let index = createIndex(children);
  
  is.equal(index.size, 3);

  is.equal(index.get(A), 0);
  is.equal(index.get(B), 1);
  is.equal(index.get(C), 2);
});
